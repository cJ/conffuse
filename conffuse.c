// SPDX-License-Identifier: GPL-3.0-only OR Proprietary
// SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-conffuse@zougloub.eu>

#define FUSE_USE_VERSION 31

#include <fuse3/fuse.h>
#include <glib.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#define debug(...) // fprintf(stderr, __VA_ARGS__)

static GHashTable* data_map = NULL; // pid -> data

static time_t last_gen = 0;

GBytes * cache = NULL;

GMutex hash_table_mutex;


struct conffuse_options {
	int log_level;
	double cache_time;
	char const * config;
	char const * virtual_path;
} options;


static GBytes * generate_(const char* name)
{
	GBytes * ret = NULL;

	debug("Generating %s\n", name);

	const char* xdgConfigHome = getenv("XDG_CONFIG_HOME");
	char* hooksRoot;
	if (xdgConfigHome == NULL) {
		const char* home = getenv("HOME");
		if (home == NULL) {
			goto end0;
		}
		asprintf(&hooksRoot, "%s/.config/conffuse", home);
	}
	else {
		asprintf(&hooksRoot, "%s/conffuse", xdgConfigHome);
	}

	if (hooksRoot == NULL) {
		goto end0;
	}

	char* cmd;
	asprintf(&cmd, "%s/%s", hooksRoot, name);

	if (cmd == NULL) {
		goto end1;
	}

	FILE* fp = popen(cmd, "r");
	if (fp == NULL) {
		goto end2;
	}

	{
		GByteArray* byte_array = g_byte_array_new();
		gchar buffer[4096]; // Buffer for reading chunks of the file
		size_t nread;

		while ((nread = fread(buffer, 1, sizeof(buffer), fp)) > 0) {
			g_byte_array_append(byte_array, (const guint8*)buffer, nread);
		}

		ret = g_byte_array_free_to_bytes(byte_array);
	}

 end3:
	pclose(fp);
 end2:
	free(cmd);
 end1:
	free(hooksRoot);
 end0:
	return ret;
}

static GBytes* generate(const char* name)
{
	time_t now = time(NULL);
	if (cache == NULL || difftime(now, last_gen) > options.cache_time) {
		if (cache != NULL) {
			g_bytes_unref(cache);
			cache = NULL;
		}

		cache = generate_(name);
		last_gen = now;
	}

	if (cache == NULL) {
		return cache;
	}

	return g_bytes_ref(cache);
}


static int conffuse_getattr(const char* path, struct stat* st, struct fuse_file_info* fi)
{
	(void) fi;

	debug("Getattr %s\n", path);

	GBytes * data = generate(options.config);

	if (data == NULL) {
		return -1;
	}

	st->st_mode = S_IFREG | 0444;
	st->st_nlink = 1;
	st->st_size = g_bytes_get_size(data);
	st->st_uid = getuid();
	st->st_gid = getgid();
	clock_gettime(CLOCK_REALTIME, &st->st_mtim);
	clock_gettime(CLOCK_REALTIME, &st->st_ctim);
	clock_gettime(CLOCK_REALTIME, &st->st_atim);

	g_bytes_unref(data);

	return 0;
}

static int conffuse_open(const char* path, struct fuse_file_info* fi)
{
	debug("Open %s\n", path);

	if ((fi->flags & O_ACCMODE) != O_RDONLY) {
		return -EACCES;
	}

	struct fuse_context *context = fuse_get_context();
	pid_t pid = context->pid;

	GBytes * data = generate(options.config);

	if (data == NULL) {
		return -1;
	}

	g_mutex_lock(&hash_table_mutex);
	g_hash_table_insert(data_map, GINT_TO_POINTER(pid), data);
	g_mutex_unlock(&hash_table_mutex);

	fi->fh = pid;
	return 0;
}

static int conffuse_read(const char* path, char* buf, size_t size, off_t offset, struct fuse_file_info* fi)
{
	debug("Read %s\n", path);
	pid_t pid = fi->fh;
	g_mutex_lock(&hash_table_mutex);
	GBytes * data = g_hash_table_lookup(data_map, GINT_TO_POINTER(pid));
	g_mutex_unlock(&hash_table_mutex);

	size_t len;
	char const * d = g_bytes_get_data(data, &len);

	if (offset >= len) {
		return 0;
	}

	if (offset + size > len) {
		size = len - offset;
	}

	memcpy(buf, &d[offset], size);

	return size;
}

static int conffuse_release(const char* path, struct fuse_file_info* fi)
{
	debug("Release %s\n", path);
	pid_t pid = fi->fh;
	g_mutex_lock(&hash_table_mutex);
	g_hash_table_remove(data_map, GINT_TO_POINTER(pid));
	g_mutex_unlock(&hash_table_mutex);
	return 0;
}

static struct fuse_operations const oper = {
 .getattr = conffuse_getattr,
 .open = conffuse_open,
 .read = conffuse_read,
 .release = conffuse_release,
};

#define CONFFUSE_OPT_KEY(t, p, v) { t, offsetof(struct conffuse_options, p), v }

static struct fuse_opt conffuse_opts[] = {
 CONFFUSE_OPT_KEY("--log-level=%d", log_level, 0),
 CONFFUSE_OPT_KEY("--cache-time=%lf", cache_time, 0),
 FUSE_OPT_END
};

static int conffuse_opt_proc(void *data, const char *arg, int key, struct fuse_args *outargs)
{
	if (key == FUSE_OPT_KEY_NONOPT && options.config == NULL) {
		options.config = arg;
		return 0; // handled
	}
	return 1; // for libfuse
}

int main(int argc, char* argv[])
{
	int ret;

	struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

	g_mutex_init(&hash_table_mutex);

	options.log_level = 0;
	options.cache_time = 60.0; // Default cache time

	if (fuse_opt_parse(&args, &options, conffuse_opts, conffuse_opt_proc) == -1) {
		return -1;
	}

	debug("config=%s\n", options.config);
	debug("cache-time=%f\n", options.cache_time);

	data_map = g_hash_table_new_full(g_direct_hash, g_direct_equal, NULL, (GDestroyNotify)g_bytes_unref);

	ret = fuse_main(args.argc, args.argv, &oper, NULL);

	fuse_opt_free_args(&args);

	g_hash_table_destroy(data_map);

	if (cache != NULL) {
		free((void*)cache);
	}

	return ret;
}
