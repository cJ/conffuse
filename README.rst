########
Conffuse
########

This is a program that uses FUSE to make files appear with dynamic contents,
the main goal being to adjust configuration files dynamically (eg. depending
on a laptop location).

Usage
#####


.. code:: sh

   conffuse [config] <target> ...

