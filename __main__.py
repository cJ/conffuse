#!/usr/bin/env python
# SPDX-License-Identifier: GPL-3.0-only OR Proprietary
# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ-conffuse@zougloub.eu>

"""
This FUSE filesystem returns dynamic file contents.
"""


import io
import os
import sys
import time
import errno
import logging
import hashlib
import array
import threading
import subprocess

import llfuse


logger = logging.getLogger()


xdg_config_home = os.environ.get("XDG_CONFIG_HOME", os.path.expanduser("~/.config"))
hooks_root = os.path.join(xdg_config_home, "conffuse")

last_gen = 0

cache = None


def generate(name):
	logger.debug("Generate %s", name)

	global last_gen
	global cache

	now = time.time()

	if now > last_gen + args.cache_time:
		logger.info("Regenerate")
		cmd = [
		 os.path.join(hooks_root, name),
		]
		ret = subprocess.run(cmd, stdout=subprocess.PIPE)

		cache = ret.stdout
		last_gen = now

	return cache


class Filesystem(llfuse.Operations):
	def __init__(self, name):
		self.name = name
		self.data = dict() # pid -> data

	def getattr(self, inode, ctx=None):
		logger.debug("Getattr %d %s", inode, ctx.pid)
		entry = llfuse.EntryAttributes()

		data = generate(self.name)

		entry.st_uid = os.getuid()
		entry.st_gid = os.getgid()
		entry.st_mode = 33204
		entry.st_size = len(data)
		entry.st_ino = inode
		now = int(time.time() * 1e9)
		entry.st_atime_ns = now
		entry.st_mtime_ns = now
		entry.st_ctime_ns = now
		entry.generation = 0
		entry.entry_timeout = 5
		entry.attr_timeout = 5
		entry.st_blksize = 512
		entry.st_blocks = ((entry.st_size+entry.st_blksize-1) // entry.st_blksize)

		return entry

	def open(self, inode, flags, ctx):
		logger.debug("Open %s flags=%d ctx=%s", inode, flags, ctx)
		self.data[ctx.pid] = generate(self.name)
		return ctx.pid

	def release(self, fh):
		logger.debug("Release %s", fh)
		del self.data[fh]

	def read(self, fd, offset, size):
		logger.debug("Reading %s @%d, %d", fd, offset, size)
		data = self.data[fd]
		return data[offset:offset+size]


if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(
	 description="FUSE filesystem",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	parser.add_argument("--cache-time",
	 type=float,
	 default=60,
	 help="Time before regenerating",
	)

	parser.add_argument('config',
	 help="",
	)

	parser.add_argument('virtual',
	 help="",
	)

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args()

	logging.basicConfig(
	 datefmt="%Y%m%dT%H%M%S",
	 level=getattr(logging, args.log_level),
	 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
	)

	fs = Filesystem(args.config)

	llfuse.init(fs, args.virtual, [])
	try:
		llfuse.main(workers=1)
	finally:
		llfuse.close()
