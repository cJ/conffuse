from setuptools import setup, find_packages

setup(
	name="conffuse",
	packages=find_packages(include=['abcd.efgh']),
	package_dir = {'cJ.conffuse': '.'},
	py_modules=[
	 "cJ.conffuse.__init__",
	 "cJ.conffuse.resilientfifo",
	],
	install_requires=[
	 "llfuse",
	],
)
