#!/usr/bin/env python
# encoding: utf-8

def options(opt):
	opt.load('compiler_c gnu_dirs')

def configure(conf):
	conf.load('compiler_c gnu_dirs')
	conf.check_cfg(package="glib-2.0", args="--cflags --libs", uselib_store="glib2")
	conf.check_cfg(package="fuse3", args="--cflags --libs", uselib_store="fuse3")

def build(bld):
	bld(
	 target="conffuse",
	 features="c cprogram",
	 source = "conffuse.c",
	 use="fuse3 glib2",
	)
